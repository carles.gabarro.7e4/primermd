## Barcelona
---
Text amb la informació de Barcelona, bonica ciutat del Mediterrani

#### Lista ordenada de barris

1. Noubarris
2. Guinardo
3. Trinitat

#### Llista desordenada de barris

* Trinitat Nova
* Guinaueta
* Ciutat Vella

Enllaç a una web [enllaç a Barcelona](https://www.barcelona.cat/ca/)

### BARCELONA

![](barcelona-skyline.jpg)








